extern crate tera;
extern crate yaml_rust;

use std::env;
use std::fs;

use serenity::{
    async_trait,
    model::{channel::Message, gateway::Ready},
    prelude::*,
};

use tera::{Context as TeraContext, Tera};
use yaml_rust::{YamlLoader};

const CC_HEADER: &str = "-----";
const PREFIX: &str = "!";

struct Handler {
    tera: Tera,
    commands: Vec<Command>
}

#[derive(Clone)]
struct Command {
    name: String,
    description: String
}

impl Handler {
    pub fn new() -> Self {
        let mut tera = Tera::default();
        tera.register_filter("upper", Self::to_iter);
        let commands = Self::load_cc(&mut tera);

        Self {
            tera,
            commands
        }
    }

    pub fn to_iter()

    fn load_cc(tera: &mut Tera) -> Vec<Command> {
        let mut commands = vec![];
        let paths = fs::read_dir("./cc").unwrap();

        for path in paths {
            let dir_entry = path.unwrap();
            let file = fs::read_to_string(dir_entry.path()).unwrap();
            let index = file.find(CC_HEADER).unwrap();
            let docs = YamlLoader::load_from_str(&file[0..index]).unwrap();
            let doc = &docs[0];

            commands.push(Command {
                name: doc["name"].as_str().unwrap().to_owned(),
                description: doc["description"].as_str().unwrap().to_owned()
            });
            tera.add_raw_template(dir_entry.file_name().to_str().unwrap(), &file[index+CC_HEADER.len()..]).unwrap();
        }

        commands
    }
}

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content.starts_with(PREFIX) {
            let mut context = TeraContext::new();
            context.insert("user", &msg.author);
            for command in &self.commands {
                let cmd = format!("{}{}", PREFIX, command.name);
                if !msg.content.starts_with(&cmd) {
                    continue
                }
                let args = shellwords::split(&msg.content[cmd.len()..]).unwrap();
                context.insert("args", &args);
                match self.tera.render(&*format!("{}.tmpl", command.name), &context) {
                    Ok(output) => if let Err(why) = msg.channel_id.say(&ctx.http, output).await {
                        println!("Error sending message: {:?}", why);
                    }
                    Err(tera_why) => if let Err(why) = msg.channel_id.say(&ctx.http, format!("{:?}", tera_why)).await {
                        println!("Error sending message: {:?}", why);
                    }
                }
                if let Ok(output) = self.tera.render(&*format!("{}.tmpl", command.name), &context) {
                    if let Err(why) = msg.channel_id.say(&ctx.http, output).await {
                        println!("Error sending message: {:?}", why);
                    }
                }
            }
        }
    }

    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}

#[tokio::main]
async fn main() {
    let token = env::var("DISCORD_TOKEN")
        .expect("Expected a token in the environment");

    // let docs = YamlLoader::load_from_str(s).unwrap();

    let mut client = Client::builder(&token)
        .event_handler(Handler::new())
        .await
        .expect("Err creating client");

    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}